package z4;

import java.io.File;

import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import z4.builder.BuildException;
import z4.builder.lang.JavaLibraryBuilder;

/**
 * Client library builder based on the z4builder library.
 * 
 * This Mojo builds specifically the resources during the generate resources
 * phase.
 * 
 * @author arnaud
 *
 */
@Mojo(name = "z4-build-resources", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class Z4ResourcesBuilderMojo extends AbstractMojo {

	final Logger logger = LoggerFactory.getLogger(Z4ResourcesBuilderMojo.class);

	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;

	@Parameter(defaultValue = "${project.build.directory}", readonly = true, required = true)
	File target;

	@Parameter(property = "modelDirectory", readonly = true)
	String modelDirectory;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		String generatedResourcesFolder = target.getAbsolutePath() + "/generated-sources";

		try {
			new JavaLibraryBuilder().buildResources(modelDirectory, generatedResourcesFolder);
		} catch (BuildException exception) {
			throw new MojoExecutionException("Error while building the library resources", exception);
		}

		generatedResourcesFolder += "/src/main/resources";

		Resource resource = new Resource();
		resource.setDirectory(generatedResourcesFolder);
		project.addResource(resource);
		logger.info("Add {} as resource", generatedResourcesFolder);
	}
}
