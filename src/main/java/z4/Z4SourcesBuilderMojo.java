package z4;

import java.io.File;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import z4.builder.BuildException;
import z4.builder.LibraryBuilderFactory;
import z4.builder.ProgrammingLanguage;

/**
 * Client library builder based on the Z4builder library.
 * 
 * This Mojo builds specifically the sources during the generate sources phase.
 * 
 * @author arnaud
 *
 */
@Mojo(name = "z4-build-sources", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class Z4SourcesBuilderMojo extends AbstractMojo {

	final Logger logger = LoggerFactory.getLogger(Z4SourcesBuilderMojo.class);

	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;

	@Parameter(defaultValue = "${project.build.directory}", readonly = true, required = true)
	File target;

	@Parameter(property = "modelDirectory", readonly = true)
	String modelDirectory;

	@Parameter(property = "language", readonly = true)
	String language;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		String generatedSourcesFolder = target.getAbsolutePath() + "/generated-sources";

		try {
			LibraryBuilderFactory.get(ProgrammingLanguage.parse(language)).buildSources(modelDirectory,
					generatedSourcesFolder);
		} catch (BuildException exception) {
			throw new MojoExecutionException("Error while building the library sources", exception);
		}

		project.addCompileSourceRoot(generatedSourcesFolder);
		logger.info("Add {} as compile source root", generatedSourcesFolder);
	}
}
