# z4builder-maven-plugin

## Description

Ce plugin Maven construit la bibliothèque cliente à partir de la définition YAML du modèle Z4.

Le travail est réalisé par la bibliothèque [z4builder](https://www.gitlab.com/arno750/z4builder).

## Utilisation

Au niveau de la bibliothèque, une section est ajoutée dans le `pom.xml` :

```xml
<project ...>
...
    <properties>
...
        <z4builder.maven.version>1.0.0</z4builder.maven.version>
    </properties>
...
    </build>
        </plugins>
...
            <plugin>
                <groupId>z4</groupId>
                <artifactId>z4builder-maven-plugin</artifactId>
                <version>${z4builder.maven.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>z4-build-sources</goal>
                            <goal>z4-build-resources</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <modelDirectory>${project.build.directory}/model</modelDirectory>
                </configuration>
            </plugin>

        </plugins>
    </build>
</project>
```

## Fonctionnement

Ce plugin Maven contient deux Mojos :

| Mojo | Phase de travail |
| :-: | :-: |
| `z4-build-sources` | `generate-sources` |
| `z4-build-resources` | `generate-resources` |

Les deux Mojo construisent les sources de la bibliothèque Z4 dans l'arborescence `${project.build.directory}/generated-sources` à partir du répertoire du modèle fourni par le paramètre de configuration `modelDirectory`.
